package com.ghoxha.web.studentmanagementsystem.entity;

import java.util.List;

public class Course {

    private int id;
    private String name;
    private int duration;
    private int etc;
    private List<Student> studentsList;

    private Course() {

    }

    public Course(String name, int duration, int etc, List<Student> studentsList) {
        this.name = name;
        this.duration = duration;
        this.etc = etc;
        this.studentsList = studentsList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getEtc() {
        return etc;
    }

    public void setEtc(int etc) {
        this.etc = etc;
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(List<Student> studentsList) {
        this.studentsList = studentsList;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                ", etc=" + etc +
                ", studentsList=" + studentsList +
                '}';
    }
}
