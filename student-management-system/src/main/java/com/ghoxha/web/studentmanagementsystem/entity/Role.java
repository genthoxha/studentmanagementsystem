package com.ghoxha.web.studentmanagementsystem.entity;

import org.hibernate.validator.internal.constraintvalidators.bv.EmailValidator;

import java.util.List;

public class Role {

    private String id;
    private String roleTitle;
    private List<Employee> employeeList;

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public Role() {

    }

    public Role(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id='" + id + '\'' +
                ", roleTitle='" + roleTitle + '\'' +
                ", employeeList=" + employeeList +
                '}';
    }
}
