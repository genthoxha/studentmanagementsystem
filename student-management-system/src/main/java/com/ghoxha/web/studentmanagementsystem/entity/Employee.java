package com.ghoxha.web.studentmanagementsystem.entity;

import java.util.List;

public class Employee {
    
    private int id;
    private String firstName;
    private String lastName;
    private List<Course> courseList;
    private String identificationCode;
    private Role role;

    public Employee() {
        
    }

    public Employee( String firstName, String lastName, List<Course> courseList, String identificationCode, Role role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.courseList = courseList;
        this.identificationCode = identificationCode;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public String getIdentificationCode() {
        return identificationCode;
    }

    public void setIdentificationCode(String identificationCode) {
        this.identificationCode = identificationCode;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", courseList=" + courseList +
                ", identificationCode='" + identificationCode + '\'' +
                ", role=" + role +
                '}';
    }
}
